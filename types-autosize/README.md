# Installation
> `npm install --save @types/autosize`

# Summary
This package contains type definitions for autosize (http://www.jacklmoore.com/autosize/).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/autosize.

### Additional Details
 * Last updated: Thu, 27 May 2021 00:01:27 GMT
 * Dependencies: none
 * Global values: `autosize`

# Credits
These definitions were written by [Aaron T. King](https://github.com/kingdango), [keika299](https://github.com/keika299), [NeekSandhu](https://github.com/NeekSandhu), and [BendingBender](https://github.com/BendingBender).
